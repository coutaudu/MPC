%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COUTAUD Ulysse                        %
% SEMTECH                               %
% Universite Grenoble Alpes             %
% ulysse.coutaud@univ-grenoble-alpes.fr %
% ucoutaud@semtech.com                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function RES = parser(FRAME, TARGET)
  global FL;
  global APL;
  global CDUL;
  
  switch (TARGET)
	 case "FRAME_COUNTER"
	   RES = FRAME(1);
	 case "FRAGMENT_COUNTER"
	   RES = FRAME(2);
	 case "PAYLOAD"
	   RES = FRAME(3:FL+2);
	 otherwise
	   error;
  endswitch
endfunction
