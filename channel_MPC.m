
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COUTAUD Ulysse                        %
% SEMTECH                               %
% Universite Grenoble Alpes             %
% ulysse.coutaud@univ-grenoble-alpes.fr %
% ucoutaud@semtech.com                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function RES  = channel_MPC(FRAMES,PER)

  global TRACE;
  global OVERLAP;
  global ERASE_RANGE;
  global ERASE_B_MIN;
  global ERASE_B_MAX;

  if OVERLAP
    for ii = 1:256
      FRAMES(32+2,:) = [];
    endfor
  endif

  if ERASE_RANGE
    for ii = ERASE_B_MIN:ERASE_B_MAX
      FRAMES(ii,:) = [];
    endfor
  endif


  
  for ii = rows(FRAMES):-1:1
    r = rand();
    if PER > r
      FRAMES(ii,:) = [];
    endif
  endfor
  RES = FRAMES;
  
endfunction
