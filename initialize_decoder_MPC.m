%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COUTAUD Ulysse                        %
% SEMTECH                               %
% Universite Grenoble Alpes             %
% ulysse.coutaud@univ-grenoble-alpes.fr %
% ucoutaud@semtech.com                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function initialize_decoder_MPC(WL, FL)

  global RESOLVE_MATRIX;
  global FRAGMENTS_MATRIX;
  global FLAGS_DELIVERY;
  global last_treated;
  global FRAG_COUNT;
  global FRAGMENTS_BUFFER;
  global FRAMES_COUNTERS;
  
  global TRACE;
  FALSE = (0==1);
  TRUE = (0==0);
  global NB_FRAG;
  global NB_RESET;
  NB_RESET++;
  ## ################################# ##
  ## ALLOCATE BUFFERS & INIT VARIABLES ##  
  ## ################################# ##

  ## nb rows min = WL * 2 car 1 systematique + 1 redondance; On a dis on fixe buffer pour corriger à 2 fenetres donc * 2 encore. nb columns idem
  RESOLVE_MATRIX = zeros(WL * 2 * 2,WL * 2);
  FRAGMENTS_MATRIX = zeros(WL*2*2,FL);
  FLAGS_DELIVERY = ones(WL*2,1);
##  for ii =1:(WL*2*2)
##    FRAMES_COUNTERS(ii,1) = -1;
##  endfor
  FRAMES_COUNTERS = zeros(WL*2*2,1);
#  DECODED_FRAGMENTS = zeros(WL*2,FL+1); # TODO -> DECODED_FRAGMENTS / DECODED_FRAGMENTS


  ## Names ## 
  # RCS = RECEIVE_COMB_SYSTEMATIC;
  # RCR = RECEIVE_COMB_REDUNDANCY;
  # FCNT= FRAME_COUNTER;
  # RDS = RECEIVE_DATA_SYSTEMATIC;
  # RDR = RECEIVE_DATA_REDUNDACY;
  
  last_treated = 255; # La trame avant la systematique 0 était la redondance 255 (pour CR 1/2).

#  DELIVERED_FRAGMENTS = [];
  FRAG_COUNT = -1;

  FRAGMENTS_BUFFER = zeros(128, FL + 1 ); #TODO préciser FL +1  doit etre FL+lengthfragcnt. 128 peut etre on peux mettre la valeur exacte (dependant du nb de fragments par trame applicative)

endfunction
