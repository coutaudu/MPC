% AESTEST
% AES test script.
% AES test in accordance with
% Morris Dworkin, Recommendation for Block Cipher Modes of Operation
% Methods and Techniques
% NIST Special Publication 800-38A, 2001 Edition
% Appendix F: Example Vectors for Modes of Operation of the AES

% Stepan Matejka, 2011, matejka[at]feld.cvut.cz
% $Revision: 1.1.0 $  $Date: 2011/10/12 $

% plain text = 64 bytes / four 128-bits blocks
% 6bc1bee22e409f96e93d7e117393172a
% ae2d8a571e03ac9c9eb76fac45af8e51
% 30c81c46a35ce411e5fbc1191a0a52ef
% f69f2445df4f9b17ad2b417be66c3710
%pkg load communications;
Source={'73' '0' '0' '0' '0' '0' '0' '0' '0' '0' '1' '0' '0' '0' '0' '7'}; #...
#	     '1' '71' '72' '73' '74' '75' '76' '80' '0' '0' '0' '0' '0' '0' '0'  '0'}
pt = hex2dec(Source);

keyh = {'00' '00' '00' '00' '00' '00' '00' '00'...
    '00' '00' '00' '00' '00' '00' '00' '00'};
key = hex2dec(keyh);


% ------------------------------------------------------------------------
% CBC test of AES-128

initialize_vector_hexadecimal = {'00' '00' '00' '00' '00' '00' '00' '00'...
    '00' '00' '00' '00' '00' '00' '00' '00'};
initialize_vector = hex2dec(initialize_vector_hexadecimal);




aesCipher = aesinit(key);
##aesinfo(aesCipher)
k0 = hex2dec(keyh);
k0 = aes(aesCipher, 'enc', 'cbc', k0, initialize_vector);
k0

k1 = doubleSubKey(k0)
k2 = doubleSubKey(k1)

ct = aes(aesCipher, 'enc', 'cbc', pt, initialize_vector);
##aesinfo(aesCipher)

##ct
