## SEMTECH
## author: Ulysse COUTAUD


function ret = AesCmacComputeHash(AppCnt, payload)

  BLOCK_SIZE=16;

  global aesCipher;
  global k0;
  global k1;
  global k2;

  applicativeCounter3 = bitshift(bitand(AppCnt,bitshift(255,0)),0);
  applicativeCounter2 = bitshift(bitand(AppCnt,bitshift(255,8)),-8);
  applicativeCounter1 = bitshift(bitand(AppCnt,bitshift(255,16)),-16);
  applicativeCounter0 = bitshift(bitand(AppCnt,bitshift(255,24)),-24);

  source(1) = 0x49;
  source(11)=applicativeCounter3;
  source(12)=applicativeCounter2;
  source(13)=applicativeCounter1;
  source(14)=applicativeCounter0;
  
  source(16)=columns(payload)+1;
  source(17)=applicativeCounter3;

  for i=1:columns(payload)
    source(17+i)=payload(i);
  endfor

% CBC test of AES-128

  ## Constructor
  macLength=BLOCK_SIZE;
  buffer = zeros(1,BLOCK_SIZE);
  ## end constructor


  initialize_vector_hexadecimal = {'00' '00' '00' '00' '00' '00' '00' '00' '00' '00' '00' '00' '00' '00' '00' '00'};
  initialize_vector = hex2dec(initialize_vector_hexadecimal);
#  aesCipher = aesinit(key);
#  aesCipher = aesinit(key);
  bufferCount=0;
  ## end init

  ## CalculateHash

  ## update block
  data=source;
  currentOffset = 0;
  dataLength = columns(data);


  ##// Transform all the full blocks in data
  buffer=initialize_vector;

  while currentOffset+BLOCK_SIZE < dataLength
    ##  aesCipher.update(data, currentOffset, BLOCK_SIZE, buffer, 0);

    dataIn = data(currentOffset+1:currentOffset+BLOCK_SIZE);
    buffer = aes(aesCipher, 'enc', 'cbc', dataIn, buffer);
    bufferAes=buffer;
    currentOffset+=BLOCK_SIZE;
  endwhile

  ## Save the leftover bytes to buffer
  if (currentOffset != dataLength)
    length=dataLength-currentOffset;
    srcPos=currentOffset+1;
    buffer(1:length)=data(srcPos:srcPos+length-1);
#  System.arraycopy(data, currentOffset, buffer, 0, data.length-currentOffset);
    bufferCount = dataLength-currentOffset;
  endif


  ## doFinal
  if (bufferCount < BLOCK_SIZE) 
    ## Add padding and XOR with k2 instead
    _0x80=hex2dec('80');
    buffer(bufferCount+1) = _0x80;
    for i=bufferCount+1+1:BLOCK_SIZE
      buffer(i) = 0;      
    endfor
    subKey=k2;
  else
    subKey=k1;
  endif
 
  for i=1:BLOCK_SIZE
    buffer(i) = bitxor(buffer(i),subKey(i));
  endfor

  buffer = aes(aesCipher, 'enc', 'cbc', buffer, bufferAes);

  ret = buffer;

endfunction
