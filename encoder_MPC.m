%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COUTAUD Ulysse                        %
% SEMTECH                               %
% Universite Grenoble Alpes             %
% ulysse.coutaud@univ-grenoble-alpes.fr %
% ucoutaud@semtech.com                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function code = encoder_MPC (SYSTEMATICS_FRAGMENTS, WL)
  global TRACE;
  global NB_FRAG_BY_INTEGRITY_DU;
  global NUMBER_FRAGMENT_MAX;
  VERBOSE=(0==1);
  
  MAX_SYSTEMATIC_FRAGMENT_NUMBER = NUMBER_FRAGMENT_MAX; 
  
  NB_FRAGMENTS_EXP = rows(SYSTEMATICS_FRAGMENTS)*2; # *2 car codage 1/2, peut etre puncturé dans un second temps
  NB_ADU_EXP =  rows(SYSTEMATICS_FRAGMENTS) / NB_FRAG_BY_INTEGRITY_DU;

  for (integrityDataUnitIndex = 0:NB_ADU_EXP-1) #/!\ tableaux a partir de 1
### Partie Systematique
    if VERBOSE
      disp('************************');
      disp([ 'Applicative Data Unit :',num2str(integrityDataUnitIndex) ]);
    endif

    for (fragmentPositionInIntegrityDU = 0:NB_FRAG_BY_INTEGRITY_DU-1)
      fragmentPositionInOutputFlow = ((integrityDataUnitIndex)*NB_FRAG_BY_INTEGRITY_DU*2+(fragmentPositionInIntegrityDU));
      if(VERBOSE)
	disp(['   Fragment position in Integrity Data Unit: ', num2str(fragmentPositionInIntegrityDU) ]);
	disp(['   Fragment position in output: ', num2str(fragmentPositionInOutputFlow) ]);
      endif
      ALL_FRAGMENTS(fragmentPositionInOutputFlow+1,1) = mod((integrityDataUnitIndex*NB_FRAG_BY_INTEGRITY_DU)+fragmentPositionInIntegrityDU,MAX_SYSTEMATIC_FRAGMENT_NUMBER+1); ## Fragments systematiques sont numerotées de 0 à 127
      systematic_fcnt=integrityDataUnitIndex*NB_FRAG_BY_INTEGRITY_DU+fragmentPositionInIntegrityDU; 
      for jj = 1: columns(SYSTEMATICS_FRAGMENTS) # Remplis avec le App Data Unit
	ALL_FRAGMENTS(fragmentPositionInOutputFlow+1,jj+1) = SYSTEMATICS_FRAGMENTS(systematic_fcnt+1,jj);
      endfor
      
    endfor
    
    if VERBOSE
      disp('   ---');
    endif

### Partie Redondante
    for (redundancyPositionInIntegrityDU = 0:NB_FRAG_BY_INTEGRITY_DU-1)
      fragmentPositionInOutputFlow = ((integrityDataUnitIndex)*NB_FRAG_BY_INTEGRITY_DU*2+(redundancyPositionInIntegrityDU))+NB_FRAG_BY_INTEGRITY_DU;
      if VERBOSE
	disp(['   Redundancy local index: ', num2str(redundancyPositionInIntegrityDU) ]);
	disp(['   Fragment position in output: ', num2str(fragmentPositionInOutputFlow) ]);
      endif
      redundancy_fcnt = mod((integrityDataUnitIndex*NB_FRAG_BY_INTEGRITY_DU)+redundancyPositionInIntegrityDU,MAX_SYSTEMATIC_FRAGMENT_NUMBER+1)+128; ## Les trames de code sontnumerotées de 128 à 255
      ALL_FRAGMENTS(fragmentPositionInOutputFlow+1,1) = redundancy_fcnt; 
      RC = get_random_combinaison(WL,redundancy_fcnt-128);
      if(VERBOSE)
	RC
	fprintf("   Fragment [%d] <- xor(", redundancy_fcnt);
      endif
      for (j_red = 1:columns(RC))
	if (RC(j_red) == 1)
	  includedFragmentOriginalPosition = (systematic_fcnt + (j_red - WL))+1;
	  if (includedFragmentOriginalPosition > 0)
	    if(VERBOSE)
	      fprintf("#%d ", includedFragmentOriginalPosition);
	    endif
	    for jj = 1:columns(SYSTEMATICS_FRAGMENTS)
	      ALL_FRAGMENTS(fragmentPositionInOutputFlow+1,jj+1) = bitxor(ALL_FRAGMENTS(fragmentPositionInOutputFlow+1,jj+1), SYSTEMATICS_FRAGMENTS(includedFragmentOriginalPosition,jj) );
	    endfor
	  endif
	endif
      endfor
      if(VERBOSE)
	fprintf(")\n");
      endif
    endfor
  endfor

  code=ALL_FRAGMENTS;

endfunction
