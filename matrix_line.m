%returns the line N containing M (0 or 1s) of the encoding parity check matrix
%r is a vector of 1/0 of size M
function matrix_line = matrix_line(N,M)

  global ratioInclude;


  matrix_line = zeros(1,M);
s=0;

% m = 2^(floor(log2(M-1))+1); %power of 2 immediately superior to M;

if (M == 2^floor(log2(M))) % if M is a power of 2
    m=1;
else
    m=0;
end


x= 1+1001*N;
ratioIncludeValue=floor(M/ratioInclude);
nb_coeff=0;
while (nb_coeff<ratioIncludeValue)
    r=2^16;
    while (r>=M)
        x=prbs23(x);
        r=mod(x, M+m); % on fait modulo M si M n'et pas une puissnace de 2 , sinon M+1
    end
    
    matrix_line(r+1) = 1; %set to 1 the column which were randomly selected
    nb_coeff = nb_coeff + 1;
    
    
end
