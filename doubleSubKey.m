function ret = doubleSubKey(key)

  ##  boolean firstBitSet = ((key[0]&0x80) != 0);
  firstBitSet = (bitand(key(1),0x80)!=0);
  
  klength = columns(key);
  
  for i = 1:klength
    ret(i)= bitand(bitshift(key(i),1),0xFF);
    if (i+1<=klength && (bitand(key(i+1),0x80)!=0))
      ret(i) = bitor(ret(i),0x1);
    endif

  endfor
  if firstBitSet
    ret(klength) = bitxor(ret(klength),0x87); #0x87 is CONSTANT. 
  endif
  
endfunction
