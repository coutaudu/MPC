%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COUTAUD Ulysse                        %
% SEMTECH                               %
% Universite Grenoble Alpes             %
% ulysse.coutaud@univ-grenoble-alpes.fr %
% ucoutaud@semtech.com                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [DELIVERED_DATA ERRORS AppCntMSB] = integrity(RECOVERED_DATA, AppCntMSB, WL)
  ERRORS = 0;
  DELIVERED_DATA = [];
  TRUE = (0==0);
  FALSE = (1==0);
  global APP_COUNT;
  global CURRENT_RECEPT
  global TOTAL_LAT;
  
  for ii = 1:rows(RECOVERED_DATA)
    
    APP_CNT_lsb = RECOVERED_DATA(ii,1);
    
    if test_hash(RECOVERED_DATA(ii,:), AppCntMSB);
				#	disp("HASH OK!");
      APP_COUNT = APP_CNT_lsb + (256*AppCntMSB);
      LAT = CURRENT_RECEPT - APP_COUNT ;
      while LAT < 0
	CURRENT_RECEPT = CURRENT_RECEPT + 128;
	LAT = CURRENT_RECEPT - APP_COUNT;
      endwhile
      TOTAL_LAT = TOTAL_LAT + LAT;
      DELIVER_DATA = RECOVERED_DATA(ii,:);
      DELIVERED_DATA = [ DELIVERED_DATA ; DELIVER_DATA ];
      else
	resynchro = FALSE;
	for jj = 1:10 #NB de wrap maximum toleré, sera borné par difference avec lorawan frame counter
	  if test_hash(RECOVERED_DATA(ii,:), AppCntMSB+1); 
	    APP_COUNT = APP_CNT_lsb + (256*(AppCntMSB+1));
	    LAT = CURRENT_RECEPT - APP_COUNT;
	    while LAT < 0
	      CURRENT_RECEPT = CURRENT_RECEPT + 128;
	      LAT = CURRENT_RECEPT - APP_COUNT;
	    endwhile
		    
	    TOTAL_LAT = TOTAL_LAT + LAT;
	    resynchro = TRUE;
		    #	  disp("	BUT OK IT'S JUST A WRAP !");
	    DELIVER_DATA = RECOVERED_DATA(ii,:);
	    DELIVERED_DATA = [ DELIVERED_DATA ; DELIVER_DATA ];
	    AppCntMSB = AppCntMSB + (jj-1);
	    if APP_CNT_lsb>2*WL
	      ## disp("Increment AppCntMSB");
	      AppCntMSB++;
	    endif
	  endif
	  if resynchro
	    break;
	  endif
	endfor
	if !resynchro
	  ##Flush Buffers
	  ERRORS = 1; ## On ne signla pas combien yen a eu exactement, juste que cette fenetre était mauvaise.
	  ##	    ans = input("Press any key to continue");
	else

	endif
    endif

  endfor
  
endfunction
