%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COUTAUD Ulysse                        %
% SEMTECH                               %
% Universite Grenoble Alpes             %
% ulysse.coutaud@univ-grenoble-alpes.fr %
% ucoutaud@semtech.com                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

				% Encoder Multiple Parity Check

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
####################### ################ #######################
####################### ###  SENDER  ### #######################
####################### ################ #######################
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function CODED_FRAMES = sender_MPC(ENCODED_FRAGMENTS_FLOW, WL, FL)
  global TRACE;
  global writeRedondancy;
  counter=0;
  for ii = 1:rows(ENCODED_FRAGMENTS_FLOW)
#    CODED_FRAMES(ii,:) = [(ii-1), ENCODED_FRAGMENTS_FLOW(ii,:)]; #Ajoute LORAWAN_FRM_COUNTER
    if ( (writeRedondancy) || (mod(ii,2)==1) )
      disp(["{\"hardware_serial\":\"70B3D57ED000FF51\",\"port\":201,\"counter\":",num2str(counter),",\"payload_raw\":{\"data\":[", num2str(ENCODED_FRAGMENTS_FLOW(ii,:),"%d,"),"]}}" ]);
      counter++;
    endif
  endfor
  
  disp(["{\"hardware_serial\":\"70B3D57ED000FF51\",\"port\":201,\"counter\":",num2str(counter),",\"payload_raw\":{\"data\":[]}}" ]);  
endfunction
