
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COUTAUD Ulysse                        %
% SEMTECH                               %
% Universite Grenoble Alpes             %
% ulysse.coutaud@univ-grenoble-alpes.fr %
% ucoutaud@semtech.com                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function FRAGMENTS_UNITS = fragmentation( DATA_UNITS, FL)
  global NB_FRAG;
  
  integrityLayerDataUnitLength = columns(DATA_UNITS);
  ##
  ## FRAGMENTE ##############################################################
  ##
  yy = 1;
  for ii = 1:rows(DATA_UNITS)
    for jj = 1: NB_FRAG
      borne_inf = (jj-1) * FL + 1;
      borne_max = min((jj) * FL,columns(DATA_UNITS));
      ll = 0;
      for kk = borne_inf:borne_max
	ll++;
	FRAGMENTS_UNITS(yy,ll) = DATA_UNITS(ii,kk);
     endfor
      yy++;
    endfor
  endfor
  ## #############################################################################


endfunction
