#/bin/bash

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#% COUTAUD Ulysse                        %
#% SEMTECH                               %
#% Universite Grenoble Alpes             %
#% ulysse.coutaud@univ-grenoble-alpes.fr %
#% ucoutaud@semtech.com                  %
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


echo "[1] No counter overflow. No fragmentation."
octave-cli MPC.m 8 64 7 0.2  > /tmp/RES.log
var=`cat /tmp/RES.log`
if [ $var=="NOERROR" ]
then
    echo "[PASS]"
else
    echo "[FAIL]"
fi

echo "[2] Counter overflow. No fragmentation."
octave-cli MPC.m 8 555 7 0.2  > /tmp/RES.log
var=`cat /tmp/RES.log`
if [ $var=="NOERROR" ]
then
    echo "[PASS]"
else
    echo "[FAIL]"
fi

echo "[3] No Counter overflow. Fragmentation without padding."
octave-cli MPC.m 8 64 17 0.2  > /tmp/RES.log
var=`cat /tmp/RES.log`
if [ $var=="NOERROR" ]
then
    echo "[PASS]"
else
    echo "[FAIL]"
fi

echo "[4] No Counter overflow. Fragmentation with padding."
octave-cli MPC.m 8 64 15 0.2  > /tmp/RES.log
var=`cat /tmp/RES.log`
if [ $var=="NOERROR" ]
then
    echo "[PASS]"
else
    echo "[FAIL]"
fi

echo "[5] Counter overflow. Fragmentation with padding."
octave-cli MPC.m 8 300 15 0.2  > /tmp/RES.log
var=`cat /tmp/RES.log`
if [ $var=="NOERROR" ]
then
    echo "[PASS]"
else
    echo "[FAIL]"
fi


echo "[6] Counter overflow with overlap. No fragmentation."
octave-cli MPC.m 8 300 7 0.2 OVERLAP > /tmp/RES.log
var=`cat /tmp/RES.log`
if [ $var=="RESET" ]
then
    echo "[PASS]"
else
    echo "[FAIL]"
fi

echo "[7] Test(1) Erasure Correction Performances."
octave-cli MPC.m 8 300 7 0.1 TEST_ECC_PERF > /tmp/RES.log
var=`cat /tmp/RES.log`
if [ $var==">95%" ]
then
    echo "[PASS]"
else
    echo "[FAIL]"
fi

echo "[8] Test(2) Erasure Correction Performances."
octave-cli MPC.m 16 500 7 0.4 TEST_ECC_PERF > /tmp/RES.log
var=`cat /tmp/RES.log`
if [ $var==">95%" ]
then
    echo "[PASS]"
else
    echo "[FAIL]"
fi


echo "[9] Test(3) Erasure Correction Performances."
octave-cli MPC.m 16 500 7 0.7 TEST_ECC_PERF > /tmp/RES.log
var=`cat /tmp/RES.log`
if [ $var=="PDR~PER" ]
then
    echo "[PASS]"
else
    echo "[FAIL]"
fi

echo "[10] RESET."
octave-cli MPC.m 16 500 7 0.1 SIMU_RESET 100 > /tmp/RES.log
var=`cat /tmp/RES.log`
if [ $var=="NOERROR" ]
then
    echo "[PASS]"
else
    echo "[FAIL]"
fi
