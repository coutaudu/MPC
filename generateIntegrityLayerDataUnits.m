
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
####################### ################ #######################
####################### ###  SENDER  ### #######################
####################### ################ #######################
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


# ajouter les en-têtes
#    AppCnt
#    hash
# calculer taille des fragments
function retval = generateIntegrityLayerDataUnits(NB_FRM_EXP, APL)
  global TRACE;
  global HASH_LENGTH;
  global PADDING;
  global CDUL;
  AppCntLSB = 0;
  AppCnt = 0;
  HL = HASH_LENGTH;
  ByteCnt=0;

  ## init
  keyh = {'00' '00' '00' '00' '00' '00' '00' '00'...
	       '00' '00' '00' '00' '00' '00' '00' '00'};
  key = hex2dec(keyh);
  initialize_vector_hexadecimal = {'00' '00' '00' '00' '00' '00' '00' '00' '00' '00' '00' '00' '00' '00' '00' '00'};
  initialize_vector = hex2dec(initialize_vector_hexadecimal);

  global aesCipher = aesinit(key);
  global k0;
  global k1;
  global k2;
  k0 = hex2dec(keyh);
  k0 = aes(aesCipher, 'enc', 'cbc', k0, initialize_vector); ## aesCipher.update(k0, 0, k0.length, k0, 0);
  k1 = doubleSubKey(k0);
  k2 = doubleSubKey(k1);

  for i = 1:NB_FRM_EXP
#    disp('---');
    AppCntLSB = mod(AppCnt,256);
    DATA(i,1) = AppCntLSB;
    
    for j = 1+1:APL+1
      DATA(i,j) = ('A') + mod(ByteCnt,26);
      ByteCnt++;
    endfor

    DATA(i,2:APL+1);
#       disp('...');
    HASH256 = AesCmacComputeHash(AppCnt, DATA(i,2:APL+1));
    #HASH256= [1 0];
#    disp('~~~');
    
    H1 = HASH256(1:2);
    
 #   H2 = hex2dec(HASH256(3:4));
    DATA(i,APL+2:APL+3) = H1;
#    DATA(i,APL+3) = H2;
    DATA(i,:);
   

    for j = 1:PADDING
      DATA(i, CDUL+j)=0;
    endfor
    
    AppCnt++;
  endfor
  
  retval = DATA;
endfunction
