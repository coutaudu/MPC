%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COUTAUD Ulysse                        %
% SEMTECH                               %
% Universite Grenoble Alpes             %
% ulysse.coutaud@univ-grenoble-alpes.fr %
% ucoutaud@semtech.com                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% 1 Si trame numero colonne incluse, 0 sinon.
function random_combinaison = get_random_combinaison(WINDOW_LENGTH, SEQ_NUM)
  global CR_numerateur;
  global CR_denominateur;
  
%%%%%%%%%%%
% ENCODER %
%%%%%%%%%%%
  lig_mat = mod(SEQ_NUM,128);
  random_combinaison = matrix_line(lig_mat,WINDOW_LENGTH);

endfunction
