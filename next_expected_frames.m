%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COUTAUD Ulysse                        %
% SEMTECH                               %
% Universite Grenoble Alpes             %
% ulysse.coutaud@univ-grenoble-alpes.fr %
% ucoutaud@semtech.com                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function cnt_next_expected = next_expected_frames(last_treated)

if last_treated > 127 #Derniere trame recue etait systematique
  cnt_next_expected = mod( (last_treated - 128) + 1, 128); # On s'attend à la redondante suivante
  else #Derniere trame recue etait redondance
    cnt_next_expected = mod(last_treated, 128) + 128;  #On s'attend à la systematique suivante
  endif
endfunction
