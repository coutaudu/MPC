%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COUTAUD Ulysse                        %
% SEMTECH                               %
% Universite Grenoble Alpes             %
% ulysse.coutaud@univ-grenoble-alpes.fr %
% ucoutaud@semtech.com                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function  resolver_MPC( INCOMING_FRAME)
 
  global TRACE;
  global PAUSE;
  FALSE = (0==1);
  TRUE = (0==0);

  global NB_PAQUETS_ABANDONNES;
  global WL;

  global RESOLVE_MATRIX;
  global FRAGMENTS_MATRIX;
  global FLAGS_DELIVERY;
  global last_treated;
  global FRAG_COUNT;
  global FRAGMENTS_BUFFER;
  global FRAMES_COUNTERS;

  global CURRENT_RECEPT;
  global TOTAL_LAT;
  
  ## RESOLVE_MATRIX = zeros(1);
  ## RCS = RECEIVE_COMB_SYSTEMATIC;
  ## RCR = RECEIVE_COMB_REDUNDANCY;
  ## FCNT= FRAME_COUNTER;
  ## RDS = RECEIVE_DATA_SYSTEMATIC;
  ## RDR = RECEIVE_DATA_REDUNDACY;

  DECODED_FRAGMENTS = zeros(WL*2,columns(FRAGMENTS_MATRIX));

  FCNT = parser(INCOMING_FRAME,"FRAGMENT_COUNTER");
  
##################################################################
# EVALUER QUELS FRAGMENTS SONT RECUS (et lesquels ont été perdus) #
# FAIRE GLISSER LES MATRICES DU DECODEUR                         #
##################################################################
  
  do     ## On décale les matrices de décodages 
	 expected = next_expected_frames(last_treated);
	 RESOLVE_MATRIX = shift_up(RESOLVE_MATRIX); # Dans tout les cas on shift RESOLV d'un cran vers le haut
	 FRAMES_COUNTERS = shift_up(FRAMES_COUNTERS);
	 FRAGMENTS_MATRIX = shift_up(FRAGMENTS_MATRIX); #Idem pour DATA
	 if expected < 128 #Trame systematique -
	     # Si le shift nous fait abandonner le recover d'une trame
	   FRAG_COUNT++;
	   if FLAGS_DELIVERY(1) == 0
	     NB_PAQUETS_ABANDONNES++;
	     TOTAL_LAT= TOTAL_LAT+ (WL*2);
	   endif
	   FLAGS_DELIVERY =  shift_up(FLAGS_DELIVERY); # On fait le decalage a gauche de RESOLV
	   RESOLVE_MATRIX = erase_corrupted_line(RESOLVE_MATRIX); # On supprime les lignes contenant dans leur combinaison, une trame que l'on discard
	   RESOLVE_MATRIX = shift_left(RESOLVE_MATRIX);# On slide les flags de delivery.
	 endif
	 if (FCNT != expected) #Le cnt du frag reçu n'est pas celui attendu. Elle a été perdue.
	   last_treated = expected;
	   error(['Erasure [', num2str(expected), ']']);
	   trace_resolver_data_struct(FLAGS_DELIVERY, zeros(rows(FLAGS_DELIVERY)), FRAGMENTS_MATRIX, RESOLVE_MATRIX, DECODED_FRAGMENTS);
	   if TRACE
	     head('');
	     if PAUSE
	       ##ans = input("Press any key to continue");
	       ## pause;
	     endif
	   endif

	 endif

  until ( FCNT == expected)

  last_treated = FCNT;

# TODO check longueur de la trame reçue, pour évaluer combien de ragments sont contenus.
# Si plusieurs fragments dans la trame, les traiter un par un (slides + OP sur les matrices)
  
##################################################################
# TRAITER LA TRAME RECUE                                         #
# APPLIQUER LES OPERATIONS NECESSAIRES SUR LES MATRICES          #
# RECONSTITUER ET DELIVRER LES NOUVELLES TRAMES A LA COUCHE SUP  #
##################################################################

  PAYLOAD = parser(INCOMING_FRAME,"PAYLOAD");
  FRAME_COUNTER = parser(INCOMING_FRAME,"FRAME_COUNTER");
  FRAMES_COUNTERS(end,:) = FRAME_COUNTER;
  if FCNT < 128 ## SYSTEMATIC
    CURRENT_RECEPT_NEW = FCNT;
    while  CURRENT_RECEPT_NEW < CURRENT_RECEPT
      CURRENT_RECEPT_NEW = CURRENT_RECEPT_NEW +128;
    endwhile
    CURRENT_RECEPT = CURRENT_RECEPT_NEW;

    ##TODO get_payload(INCOMING_FRAME)
##    RDS = INCOMING_FRAME(2:end); # La donnée systematique = 1er fragment recu - size(frag_CNT)
    RDS = PAYLOAD;
    RESOLVE_MATRIX(end,end) = 1; # Jintegre la trame recu à la matrice de resolution
    FRAGMENTS_MATRIX(end,:) = RDS; # J'integre les données reçues dans le buffer de decodage
    str = ['Receive [', num2str(expected), '] [',num2str(RDS),']'];
    success(str);
    
  else ## REDUNDANT

    CURRENT_RECEPT_NEW = FCNT-128;
    while  CURRENT_RECEPT_NEW < CURRENT_RECEPT
      CURRENT_RECEPT_NEW = CURRENT_RECEPT_NEW +128;
    endwhile
    CURRENT_RECEPT = CURRENT_RECEPT_NEW;

    
    RDR = PAYLOAD;
    RCR = zeros(1,WL*2);
    RCR(1,WL+1:end) = get_random_combinaison(WL,FCNT); # Le combinaison aléatoire correcpondant à la trame reçue.
    if TRACE
      disp('');
    endif
    str = ['Receive [', num2str(expected), '] [',num2str(RCR(1,WL+1:end)),'][',num2str(RDR),']'];
    success(str);

    # TRAITE LA DONNEES RECU DANS LA COMBINAISON ALEATOIRE DE LA TRAME
    [RCR RDR] = simplify_receive_combination(RCR, RDR, RESOLVE_MATRIX, FRAGMENTS_MATRIX);
    ## Si il reste au moins un "1" ? Je le place dans la matrice
    [RESOLVE_MATRIX FRAGMENTS_MATRIX] = insert_combination(RCR, RDR, RESOLVE_MATRIX, FRAGMENTS_MATRIX);
    
  endif
  
  ## Tente de simplifier la matrice par pivot de Gauss  
  [RES DECODED_FRAGMENTS] = gauss_MPC(RESOLVE_MATRIX, FRAGMENTS_MATRIX, FRAG_COUNT);
  
  ## ################### ##
  ##  DELIVER FRAGMENTS  ##
  ## ################### ##
  DELIVER_CNT = 1;
  DELIVERED_FRAGMENTS = [];

  if TRACE
    disp('');
    disp("Deliver fragment: ");
  endif

  for ii = 1:rows(RES)
    if (sum(RES(ii,:)~=0)==1)
      if !FLAGS_DELIVERY(ii) && any(DECODED_FRAGMENTS(ii,2:end))
	FLAGS_DELIVERY(ii)=TRUE; #Marque le fragment comme delivré
  	DELIVER_FRAGMENT = DECODED_FRAGMENTS(ii,:); # Delivre la donnée

  	for ii = 1:columns(DELIVER_FRAGMENT)
  	  DELIVERED_FRAGMENTS(DELIVER_CNT,ii) = DELIVER_FRAGMENT(ii); #Ajoute aux données délivrée
  	endfor
  	if TRACE
  	  yellow(num2str(DELIVER_FRAGMENT));
  	endif
  	DELIVER_CNT++;
      endif
    endif
  endfor
  if TRACE
    disp('');
  endif

  
  trace_resolver_data_struct(FLAGS_DELIVERY, RES, FRAGMENTS_MATRIX, RESOLVE_MATRIX, DECODED_FRAGMENTS);
  trace_resolver_results(FLAGS_DELIVERY, RES, FRAGMENTS_MATRIX, RESOLVE_MATRIX, DECODED_FRAGMENTS);  
  for ii = 1:rows(DELIVERED_FRAGMENTS)
    frag_cnt = DELIVERED_FRAGMENTS(ii,1);
    FRAGMENTS_BUFFER(frag_cnt+1,:) = DELIVERED_FRAGMENTS(ii,:);
  endfor

#  disp(' ');
  for ii = 1:rows(FRAGMENTS_BUFFER)
    if any(FRAGMENTS_BUFFER(ii,:))
 #     disp(num2str(FRAGMENTS_BUFFER(ii,:)," %03d"));
    endif
  endfor
  
				#  RES = DELIVERED_FRAGMENTS;

endfunction
