%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COUTAUD Ulysse                        %
% SEMTECH                               %
% Universite Grenoble Alpes             %
% ulysse.coutaud@univ-grenoble-alpes.fr %
% ucoutaud@semtech.com                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function MATRIX_OUT = shift_up(MATRIX_IN)

  MATRIX_OUT = [ MATRIX_IN; zeros(1,columns(MATRIX_IN)) ];
  MATRIX_OUT(1,:) = [];

endfunction 
  
