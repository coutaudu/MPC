%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COUTAUD Ulysse                        %
% SEMTECH                               %
% Universite Grenoble Alpes             %
% ulysse.coutaud@univ-grenoble-alpes.fr %
% ucoutaud@semtech.com                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

## Entrée: une matrice binaire CM
## CM(i,j) = 1 signifie que la trame i recue contient -notamment- la trame j
## Sortie:
##    Matrice RES(i,j) == 1 => la frame i reconstitué contient -notamment- la frame j.
##                             Si i==j && c'est le seul 0 de la ligne: alors la frame i a été reconstitué.
##    Matrice OP décrit les opérations à effectuer pour reconstituer les données. 
function [RES RES_DATA ]  = gauss_MPC(CM, DATA_MATRIX, CURRENT_INDEX)

  true = (0==0);
  false = (0==1);

  OP = zeros(columns(CM));

				#  disp('RESOLVER');
  
  %%-------------------------------------------------------------%
  ## RESOLVE WITH GUSSIAN ELIMINATION
  %%-------------------------------------------------------------%
  
for i_col = 1:columns(CM)
    
    ## Trouver la premiere ligne qui contient 1
    #    disp(CM(i_col,:))
    #    disp(CM(:,i_col));
    #    disp('---');
    frame_found = 0;
    for i_lig = i_col:rows(CM)
      if CM(i_lig,i_col) == 1
	if frame_found==0
	  #disp(['Frame[',num2str(i_col), ']trouvé dans ', num2str(i_lig), '. SWAP.']);
#	  disp(['    SWAP[',num2str(i_lig),';',num2str(i_col),']']);
	  frame_found = i_lig;
	  ## swap
#	  CM(i_lig,:) = xor (CM(i_lig,:),CM(i_col,:));
	  CM([i_col i_lig],:) = CM([i_lig i_col],:);
	  OP(i_lig,i_col) = 2;
	  #CM
	else
      	  #disp(['Frame[',num2str(i_col), ']trouvé dans ', num2str(i_lig), '. XOR']);
#	  disp(['    XOR[',num2str(i_lig),';',num2str(i_col),']']);
	  ##XOR
	  CM(i_lig,:) = xor (CM(i_lig,:),CM(i_col,:));
	  OP(i_lig,i_col) = 1;
	 # CM
	endif
      endif
    endfor
    
    if (frame_found == 0)
				#     disp('Frame was lost.');
    else
      for i_lig = 1:i_col-1
	if CM(i_lig,i_col) == 1
#disp(['Frame[',num2str(i_col),'] trouvé dans ', num2str(i_lig), '. XOR']);
#	  disp(['    XOR[',num2str(i_lig),';',num2str(i_col),']']);
	  CM(i_lig,:) = xor (CM(i_lig,:),CM(i_col,:));
	  OP(i_lig,i_col) = 1;
	#  CM
	endif
      endfor
    endif
				#    disp(CM(i_col,:))
				#    disp(CM(:,i_col));

    
    
  endfor

  OP;
  RES = CM;


  %%-------------------------------------------------------------%
  ## APPLY THE GAUSSIAN ELIMINATION TO THE DATA BUFFER 
  %%-------------------------------------------------------------%

  FL = columns(DATA_MATRIX);
  BUFFER = DATA_MATRIX;
  
  for i_col = 1:columns(OP)
			   #disp(['----Frm[',num2str(i_col),'----]']);
    for i_lig = 1:rows(OP)
      if OP(i_lig,i_col) == 2
	##SWAP
     #      disp(['    SWAP[',num2str(i_lig),';',num2str(i_col),']']);
#      disp(['    [',num2str(BUFFER(i_lig,1)),']<->[',num2str(BUFFER(i_col,1)),']']);
#      disp(['    [',num2str(BUFFER(i_lig,2)),']<->[',num2str(BUFFER(i_col,2)),']']);
	for jj =  1:FL
	  BUFFER([i_lig i_col],jj) = BUFFER([i_col i_lig],jj);
	endfor
      endif
    endfor
    for i_lig = 1:rows(OP)
      if OP(i_lig,i_col) == 1
	##XOR
      #      disp(['    XOR[',num2str(i_lig),';',num2str(i_col),']']);
#disp(['    [',num2str(BUFFER(i_lig)),']<->[',num2str(BUFFER(i_col)),']']);
	for jj =  1:FL
	  BUFFER(i_lig, jj) = bitxor(BUFFER(i_col,jj),BUFFER(i_lig,jj));
	endfor
      endif
    endfor
    
  endfor

  RES_DATA = BUFFER(1:columns(OP),:);

  insert_column = zeros(rows(RES_DATA),1);
  RES_DATA = [insert_column RES_DATA];

  for ii = rows(RES_DATA):-1:1
    RES_DATA(ii,1) = CURRENT_INDEX;
    CURRENT_INDEX--;
  endfor



endfunction
