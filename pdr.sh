#/bin/bash

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#% COUTAUD Ulysse                        %
#% SEMTECH                               %
#% Universite Grenoble Alpes             %
#% ulysse.coutaud@univ-grenoble-alpes.fr %
#% ucoutaud@semtech.com                  %
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


for per in `seq 0 0.01 0.99`
do
    echo $per
    octave-cli MPC.m 16 500 7 $per  > /tmp/RES3.log
done

