%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COUTAUD Ulysse                        %
% SEMTECH                               %
% Universite Grenoble Alpes             %
% ulysse.coutaud@univ-grenoble-alpes.fr %
% ucoutaud@semtech.com                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [RES_RM RES_DM] = insert_combination(RCR, RDR, RESOLVE_MATRIX, DATA_MATRIX);


    for ii = 1:columns(RCR)
      if RCR(ii) == 1
	RESOLVE_MATRIX(2*ii,:) = RCR;
	DATA_MATRIX(2*ii,:) = RDR;
	break
      endif
    endfor


    RES_RM = RESOLVE_MATRIX;
    RES_DM = DATA_MATRIX;

endfunction
