#!/bin/bash

##localWL=$1
##localBeginPl=$2
##localEndPl=$3


## Test sans fragmentation: Transmet 1000 octets
for localWL in 128 8
do
    localNBDATA=1000
    for inclusionRate in 0.5 0.6 0.7 0.8 0.9 1 1.5 2 4 8 10 12 15  
    do
	PL=1
	echo "octave-cli MPC.m $localWL $localNBDATA $PL $inclusionRate"
	octave-cli MPC.m $localWL $localNBDATA $PL $inclusionRate   > ./genData/ID-70B3D57ED000FF51-APL-$PL-WL-$localWL-NBDATA-$localNBDATA-INCLRATE-$inclusionRate-.input
    done
done

exit
dfz
## Test avec fragmentation maximale : NB DATA à 36 pour au final transmttre ~ 1000 octets

localNBDATA=36
for localWL in 128
do
    for inclusionRate in 1.05
    do
	PL=249
	echo "octave-cli MPC.m $localWL $localNBDATA $PL $inclusionRate"
	octave-cli MPC.m $localWL $localNBDATA $PL $inclusionRate  > ./genData/ID-70B3D57ED000FF51-APL-$PL-WL-$localWL-NBDATA-$localNBDATA-INCLRATE-$inclusionRate-.input
    done
done


## Test sans redondance
for localWL in 4
do
    localNBDATA=1000
    for inclusionRate in 5
    do
	PL=1
	echo "octave-cli MPC.m $localWL $localNBDATA $PL $inclusionRate 0"
	octave-cli MPC.m $localWL $localNBDATA $PL $inclusionRate 0  > ./genData/ID-70B3D57ED000FF51-APL-$PL-WL-$localWL-NBDATA-$localNBDATA-INCLRATE-$inclusionRate-NORED-.input
    done
done
