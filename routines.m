%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COUTAUD Ulysse                        %
% SEMTECH                               %
% Universite Grenoble Alpes             %
% ulysse.coutaud@univ-grenoble-alpes.fr %
% ucoutaud@semtech.com                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
####################### ################ #######################
####################### ### ROUTINES ### #######################
####################### ################ #######################
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

FALSE = (0==1);
TRUE = (0==0);


function infom(msg)
  global TRACE;
  if TRACE
    fprintf(1, [char(27),'[94m' msg, char(27), '[0m\n'] );
  endif
endfunction

function error(msg)
  global TRACE;
  if TRACE
    fprintf(1, [char(27),'[91m' msg, ...
		  char(27), '[0m\n'] );
  endif
endfunction

function yellow(msg)
	 fprintf(1, [char(27),  '[43m' msg, char(27), '[0m\n']);
endfunction


function success(msg)
  global TRACE;
  if TRACE
    fprintf(1, [char(27), '[32m' msg, char(27), '[0m\n']);
  endif
endfunction

function head(msg)
    fprintf(1, 
	    [char(27),  '[90m', ...
	     '==============================================================', ...
	     '========\n', ...
	     char(27), ...
	     '[0m']
	   );
    
    ML = columns(msg);
    ind = 0;
    while (ML>ind+26)
      yellow(msg(ind+1:ind+26));
      ind = ind + 26;
    endwhile
    yellow(msg(ind+1:end));
    fprintf(1, 
	    [char(27), ...
	     '[90m', ...
	     '==============================================================', ...
	     '========\n', ...
	     char(27), ...
	     '[0m']
	   );
    
 endfunction


function trace_resolver_data_struct(FLAGS_DELIVERY, RES, DATA_MATRIX, RESOLVE_MATRIX, DECODED_FRAGMENTS)
  global TRACE;
  global PAUSE;

  
  if TRACE
    disp('[Index] [RESOLVE_MATRIX] | [DATA_MATRIX]'); 
    for i = 1:rows(RESOLVE_MATRIX)
      disp([ '[', num2str(i,"%02d"), '] ' , num2str(RESOLVE_MATRIX(i,:)), ' | ', num2str(DATA_MATRIX(i,:)," %03d") ]);
    endfor
  endif
  
endfunction


function trace_resolver_results(FLAGS_DELIVERY, RES, DATA_MATRIX, RESOLVE_MATRIX, DECODED_FRAGMENTS)
  global TRACE;
  global PAUSE;
  
  if TRACE
    disp('[Delivery] [RES] [DECODED_FRAGMENTS]');
    for i = 1:rows(FLAGS_DELIVERY)
      disp([ '[', num2str(FLAGS_DELIVERY(i)), '] ' , num2str(RES(i,:)), ' | ', num2str(DECODED_FRAGMENTS(i,:)," %03d") ]);
    endfor
    
    disp('   ');
				#head(['',char(DELIVERED_DATA)]);
    head('');
    if PAUSE
      ans = input("Press any key to continue");
      ##pause;
    endif
  endif
  
endfunction

