%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COUTAUD Ulysse                        %
% SEMTECH                               %
% Universite Grenoble Alpes             %
% ulysse.coutaud@univ-grenoble-alpes.fr %
% ucoutaud@semtech.com                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function RESOLVE_MATRIX = erase_corrupted_line(RESOLVE_MATRIX)
  for ii = 1:rows(RESOLVE_MATRIX)
    if (RESOLVE_MATRIX(ii,1) == 1)
      RESOLVE_MATRIX(ii,:) = zeros(1,columns(RESOLVE_MATRIX));
    endif
  endfor
  
endfunction
