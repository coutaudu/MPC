%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COUTAUD Ulysse                        %
% SEMTECH                               %
% Universite Grenoble Alpes             %
% ulysse.coutaud@univ-grenoble-alpes.fr %
% ucoutaud@semtech.com                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%  Multiple Parity Check

## USAGE:
## octave-cli MPC.m WinLength NbPackets PayloadLength PER

clear
FALSE = (0==1);
TRUE = (0==0);
##global TRACE = TRUE;
global TRACE = FALSE;
global PAUSE = TRUE;
VERBOSE = FALSE;

global NB_RESET = 0;
global NB_PAQUETS_ABANDONNES = 0;

source("./routines.m")

### DEFAULT PROTOCOL PARAMETERS ###
global APL         = 5; # Applicative_Payload_Length
global WL          = 8;

APP_CNT_LENGTH     = 1;
global HASH_LENGTH = 2;
global ratioInclude = 2; # 2: tends to 1/2 included; 3: tends to 1/3 included ...


### DEFAULT EXPERIMENT PARAMETERS ###
NB_WIN = 1;
NB_FRAMES_EXPERIMENT = WL * NB_WIN;

### read command line arguments
if rows(argv()) >= 4
  if VERBOSE
    disp('Arguments from command line:');
  endif
  WL            = str2num(argv(){1});
  NB_FRAMES_EXPERIMENT = str2num(argv(){2});
  APL                  = str2num(argv(){3});
  ratioInclude         = str2num(argv(){4});
endif

global writeRedondancy = TRUE;
if rows(argv()) >= 5
  if (str2num(argv(){5})==0)
    writeRedondancy = FALSE;
  endif
endif


### COMPUTED VARIABLES OF THE PROTOCOL ###
global CDUL      = APL + APP_CNT_LENGTH + HASH_LENGTH; # Code_Data_Unit_Length
global NB_FRAG   = ceil(CDUL /9);
global FL        = ceil((CDUL)/NB_FRAG);
global PADDING = (NB_FRAG*FL) - CDUL;
global NB_FRAG_BY_INTEGRITY_DU = NB_FRAG;
global NUMBER_FRAGMENT_MAX = 128 - ( mod(128, NB_FRAG_BY_INTEGRITY_DU )) -1;
						     
## compute_fragment_length
if VERBOSE
  disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
  APL
  WL
  NB_FRAMES_EXPERIMENT
  CDUL
  FL
  NB_FRAG_BY_INTEGRITY_DU
  PADDING
  NUMBER_FRAGMENT_MAX
  disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
endif

%-------------------------------------------------------------------------------------------%
#                       #################
#                       ### GENERATOR ###
#                       #################
%-------------------------------------------------------------------------------------------%
integrityLayerDataUnits = generateIntegrityLayerDataUnits(NB_FRAMES_EXPERIMENT,APL); # compute AppCnt, hash
if VERBOSE
  disp(['	[', num2str(rows(integrityLayerDataUnits)), '] Application Data Units generated.']);
endif

systematicsFragments = fragmentation(integrityLayerDataUnits, FL);

systematicsAndEncodedFragments =  encoder_MPC (systematicsFragments, WL);

sender_MPC(systematicsAndEncodedFragments, WL, FL); #decoupe l'appPayLoad, compute la redondance le frag_cnt,x colle les fragments ensemble pour remplir le paquet. 

if VERBOSE
  disp(['	[', num2str(rows(systematicsAndEncodedFragments)), '] fragments sent.']);
endif


### TODO
### -> HTTP POST
