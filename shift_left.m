%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COUTAUD Ulysse                        %
% SEMTECH                               %
% Universite Grenoble Alpes             %
% ulysse.coutaud@univ-grenoble-alpes.fr %
% ucoutaud@semtech.com                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function MATRIX_OUT = shift_left(MATRIX_IN)

  MATRIX_OUT = [ MATRIX_IN, zeros(rows(MATRIX_IN),1) ];
  MATRIX_OUT(:,1) = [];

endfunction 
  
